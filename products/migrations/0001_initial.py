# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'nombre')),
                ('description', models.CharField(max_length=255, verbose_name='descripci\xf3n')),
                ('value1', models.IntegerField(default=0, verbose_name=b'valor tienda 1')),
                ('value2', models.IntegerField(default=0, verbose_name=b'valor tienda 2')),
                ('value3', models.IntegerField(default=0, verbose_name=b'valor tienda 3')),
                ('value4', models.IntegerField(default=0, verbose_name=b'valor tienda 4')),
                ('value5', models.IntegerField(default=0, verbose_name=b'valor tienda 5')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
