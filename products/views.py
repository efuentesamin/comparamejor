# Author: Edwin Fuentes Amin - efuentesamin@gmail.com
# Vistas para las peticiones REST

import sys
import time
from .models import Product
from .serializers import ProductSerializer
from rest_framework import generics
from rest_framework import mixins
from products.tasks import startTask
from celery import group



#Vistas para listado de productos o agregar uno nuevo (GET o POST)
class ProductList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
	queryset = Product.objects.all()
	serializer_class = ProductSerializer

	def get(self, request, *args, **kwargs):
		'''
			Obtener el listado de productos.
		'''
		return self.list(request, *args, **kwargs)



	def post(self, request, *args, **kwargs):
		'''
			Agregar producto.
		'''
		return self.create(request, *args, **kwargs)



#Vistas para detalle, modificacion o eliminacion de productos (GET, PUT o DELETE)
class ProductDetail(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
	queryset = Product.objects.all()
	serializer_class = ProductSerializer

	def get(self, request, *args, **kwargs):
		'''
			Obtener los detalles de un productos. Aqui se simula el llamado a los supermercados.
		'''
		print >> sys.stderr, 'Simulando consumo de servicios...'

		pk = self.kwargs['pk']
		product = Product.objects.get(id=pk)

		group([
			startTask.s(1, pk, product.value1),
			startTask.s(2, pk, product.value2),
			startTask.s(3, pk, product.value3),
			startTask.s(4, pk, product.value4),
			startTask.s(5, pk, product.value5)
		])()

		return self.retrieve(request, *args, **kwargs)



	def put(self, request, *args, **kwargs):
		'''
			Modificar un producto.
		'''
		return self.update(request, *args, **kwargs)



	def delete(self, request, *args, **kwargs):
		'''
			Eliminar un producto.
		'''
		return self.destroy(request, *args, **kwargs)
 
