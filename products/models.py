# Author: Edwin Fuentes Amin - efuentesamin@gmail.com
# -*- coding: utf-8 -*-

from django.db import models



#Modelo para representación de productos.
class Product(models.Model):
	name = models.CharField("nombre", max_length = 100)
	description = models.CharField(u"descripción", max_length = 255)
	value1 = models.IntegerField("valor tienda 1", default = 0)
	value2 = models.IntegerField("valor tienda 2", default = 0)
	value3 = models.IntegerField("valor tienda 3", default = 0)
	value4 = models.IntegerField("valor tienda 4", default = 0)
	value5 = models.IntegerField("valor tienda 5", default = 0)

	def __unicode__(self):
		return self.name
