# Author: Edwin Fuentes Amin - efuentesamin@gmail.com
# Registro de modelos para el sitio de administracion automatico.

from django.contrib import admin
from .models import Product



#Modelo Product habilitado en el sitio de administracion.
admin.site.register(Product)
