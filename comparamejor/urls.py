# Author: Edwin Fuentes Amin - efuentesamin@gmail.com
# Carga centralizada de los patrones de url's

from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers



urlpatterns = patterns('',
	url(r'^', include('products.urls')),
	url(r'^admin', include(admin.site.urls)),
)
